import groovy.io.FileType
import hudson.*
hudson.FilePath workspace = hudson.model.Executor.currentExecutor().getCurrentWorkspace()

println("Processing Applications:")

new File("${workspace}/jenkinsjobs/pipelines").eachDirMatch(~/.+\.application/)  { dir ->
  println "Processing: ${dir.name.take(dir.name.lastIndexOf('.'))}"

  new File("${workspace}/jenkinsjobs/pipelines/${dir.getName()}").eachFileMatch(~/.+\.groovy/) { file ->
    pipelineJob("${dir.name.take(dir.name.lastIndexOf('.'))}-${file.name.take(file.name.lastIndexOf('.'))}") {
      scm {
    git {
        remote {
        credentials('gitlab')
            url('ssh://git@ec2-18-188-124-175.us-east-2.compute.amazonaws.com:7999/mufg/java-hello-world.git')
        }
        branch('${repoBranch}')

    }
    }
    wrappers { preBuildCleanup() }
    definition {
        cps {
          script(readFileFromWorkspace("${WORKSPACE}/jenkinsjobs/pipelines/${dir.getName()}/${file.getName()}"))
          sandbox()
        }
        if (file.getName().contains('master')){
        triggers {
            bitbucketPush()
        }
        }
    }
    }

  }
}
