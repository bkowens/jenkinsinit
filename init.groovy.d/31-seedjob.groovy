import hudson.model.FreeStyleProject
import hudson.plugins.git.GitSCM
import hudson.plugins.git.UserRemoteConfig
import hudson.plugins.git.BranchSpec
import hudson.plugins.groovy.FileScriptSource
import hudson.plugins.groovy.Groovy
import hudson.plugins.groovy.SystemGroovy
import hudson.triggers.SCMTrigger
import javaposse.jobdsl.plugin.*
import jenkins.model.Jenkins


println "--> create z_seed-jobs"

def gitSeedScmURL = System.getenv("SEEDJOB_URL")
def gitSeedCredentialsId = System.getenv("JENKINS_GIT")
def script = new FileScriptSource("/var/jenkins_home/init.groovy.d/seedjob.groovy")
def sg = new hudson.plugins.groovy.SystemGroovy(script,null,null);

jenkins = Jenkins.instance;

jobName = "seedJob";
branch = "*/master"

jenkins.items.findAll { job -> job.name == jobName }
        .each { job -> job.delete() }

gitTrigger = new SCMTrigger("H * * * *");
dslBuilder = new ExecuteDslScripts()

dslBuilder.setTargets("**/*.groovy")
dslBuilder.setUseScriptText(false)
dslBuilder.setIgnoreExisting(false)
dslBuilder.setIgnoreMissingFiles(false)
dslBuilder.setRemovedJobAction(RemovedJobAction.DISABLE)
dslBuilder.setRemovedViewAction(RemovedViewAction.IGNORE)
dslBuilder.setLookupStrategy(LookupStrategy.SEED_JOB)

dslProject = new hudson.model.FreeStyleProject(jenkins, jobName);
dslProject.scm = new GitSCM(gitSeedScmURL);
dslProject.scm.branches = [new BranchSpec(branch)];
if (gitSeedCredentialsId != null) {
  println "--> seed jobs credentials: " + gitSeedCredentialsId
  config = new UserRemoteConfig(gitSeedScmURL, null, null, gitSeedCredentialsId)
  dslProject.scm.userRemoteConfigs = [config];
}

dslProject.addTrigger(gitTrigger);
dslProject.getBuildersList().add(sg);
dslProject.createTransientActions();

jenkins.add(dslProject, jobName);

gitTrigger.start(dslProject, true);