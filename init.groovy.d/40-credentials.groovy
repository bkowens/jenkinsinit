import jenkins.model.*
import com.cloudbees.plugins.credentials.*
import com.cloudbees.plugins.credentials.common.*
import com.cloudbees.plugins.credentials.domains.*
import com.cloudbees.plugins.credentials.impl.*
import com.cloudbees.jenkins.plugins.sshcredentials.impl.*

println "--> setting ssh creds"

def global_domain = Domain.global()

def credentialsStore = Jenkins.instance.getExtensionList('com.cloudbees.plugins.credentials.SystemCredentialsProvider'  )[0].getStore()

def seedCredsId = "jenkins"
def name = seedCredsId
def id = seedCredsId
def username = seedCredsId
def privateKey = new File("/var/jenkins_home/init.groovy.d/mufg-jenkins.key").getText()
def keySource = new BasicSSHUserPrivateKey.DirectEntryPrivateKeySource(privateKey)
def description = ""

def privateKeyFile = new File(privateKey)
if (!privateKeyFile.exists()) {
  println "WARNING: " + privateKey + " doesn't exist"
}

def credentials = new BasicSSHUserPrivateKey(
  CredentialsScope.GLOBAL,
  id,
  username,
  keySource,
  null,
  description
)

globalUnPwCredentials = new UsernamePasswordCredentialsImpl(
  CredentialsScope.GLOBAL, 
  'docker-hub-credentials', 
  'docker-hub-credentials' , 
  'bkowens', 
  '')

credentialsStore.addCredentials(global_domain, globalUnPwCredentials)

credentialsStore.addCredentials(global_domain, credentials)
