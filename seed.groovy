import hudson.FilePath
import hudson.*

hudson.FilePath workspace = hudson.model.Executor.currentExecutor().getCurrentWorkspace()

new File("${workspace}/pipelines").eachFileMatch(~/.+\.groovy/) { file ->
    println file.getName()
    pipelineJob("${file.getName()}") {
        definition {
            cps {
                script(readFileFromWorkspace(file))
                sandbox()
            }
        }
    }

}
